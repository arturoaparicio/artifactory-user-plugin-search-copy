Artifactory AQL find and copy or move items
=========
The purpose of this plugin is to allow the user to perform an AQL query and copy or move the resulting items.

Version
----

0.1


Usage
-----------

The below example assumes an admin user named admin whose password is password:

curl -X POST -uadmin:password http://localhost:8081/artifactory/api/plugins/execute/aqlcopymove -H "Content-Type: application/json" --upload-file inp.json

Where inp.json looks something like this:
```
{
    "aql": "items.find({\"$and\": [{\"repo\":{\"$eq\":\"generic\"}},{\"name\":{\"$match\":\"pac*\"}}]})",
    "path": "generic2/",
    "move": true
}
```


Input
-----------
aql - [required: String] The AQL query to find the items (must be items.find)

path - [required: String] The destination path (must be or specify a directory, not a file), ex: (repo-key/path)

move - [optional: boolean] Defaults to false (copy). true will result in a move operation

useRepoInPath [optional: boolean] Defaults to false. true will result in the path having the source repo key prepended to the path

dryRun - [optional: boolean] Defaults to false. true will result in the objects not being copied/moved.
