import groovy.json.JsonBuilder
import groovy.json.JsonSlurper

import java.awt.event.ItemEvent;
import java.sql.ResultSet

import org.artifactory.repo.RepoPathFactory
import org.artifactory.repo.Repositories;
import org.artifactory.resource.ResourceStreamHandle
import org.artifactory.storage.spring.StorageContextHelper;
import org.artifactory.aql.AqlService

/**
 * Performs an 'aql' query for items/artifacts and copies or moves the results into a repository path.
 * 
 * This execution is named 'aqlcopymove', and it will be called by REST by this
 * name.
 * 
 * Parameters:
 * aql - (required) The AQL query to find the items (must be items.find)
 * path - (required) The destination path (must be or specify a directory, not a file), ex: (repo-key/path)
 * move - (optional) Defaults to false (copy). true will result in a move operation
 * useRepoInPath - (optional) Defaults to false. true will result in the path having the source repo key prepended to the path
 * dryRun - (optional) Defaults to false. true will result in the objects not being copied/moved.
 * 
 * @author Arturo Aparicio
 */

executions {
    aqlcopymove(version:'0.1',
                   description:'Performs an AQL query for artifacts and copies/moves them.',
                   httpMethod: 'POST') 
    { params, ResourceStreamHandle body ->
        
        // Parse the input
        def reader = new InputStreamReader(body.inputStream, 'UTF-8')
        def input = null
        try {
            input = new JsonSlurper().parse(reader)
        } catch (groovy.json.JsonException ex) {
            message = "Problem parsing JSON: $ex.message"
            status = 400
            return
        }
        
        // Verify the input
        def aql = null
        def path = null
        if (!(input instanceof Map)) {
            message = 'Provided value must be a JSON object'
            status = 400
            return
        }
        if (!input['aql']) {
            message = 'The property \'aql\' must be set.'
            status = 400
            return
        }
        aql = input['aql']
        if (aql.indexOf("items.find") != 0)
        {
            message = 'The AQL query must begin with \'items.find\'.'
            status = 400
            return
        }
        if (!input['path']) {
            message = 'The property \'path\' must be set.'
            status = 400
            return
        }
        path = input['path']
		if (path[path.length() - 1] != "/")
			path += "/"
		def dryRun = ((!input['dryRun'])? false: input['dryRun'])
		def move = ((!input['move'])? false: input['move'])
		def useRepoInPath = ((!input['useRepoInPath'])? false: input['useRepoInPath'])
        // Execute and perform the move
        try {
            AqlService aqlService = StorageContextHelper.get().beanForType(AqlService.class);
            
            def results = aqlService.executeQueryLazy(aql)
            
            def resultCount = 0;
            ResultSet set = results.getResultSet()
            // Extract the SQL result
            while (set.next()) {
                ++resultCount;
                def itemRepo = set.getString(1)
                def itemPath = set.getString(2)
                def itemName = set.getString(3)
                
                def fullPath = itemRepo + "/" + ((itemPath != ".")?itemPath + "/":"") + itemName
                def sourcePath = RepoPathFactory.create(fullPath)
                def newPath = RepoPathFactory.create(path + ((useRepoInPath)?sourcePath.getRepoKey() + '/':"") + sourcePath.getPath())
				if (!dryRun) {
					if (move)
						repositories.move(sourcePath, newPath)
					else
					    repositories.copy(sourcePath, newPath)
				}
            }
            
            // Send a success response to the user
            def json = new JsonBuilder()
            json {
                success(true)
                count(resultCount)
                // For debugging
                //data(results.getFields().collect {[it]})
            }
            message = json.toPrettyString()
            status = 200
        } catch (e) {
            log.error 'Failed to execute plugin', e
            message = e.message
            status = 500
        }
    }
}